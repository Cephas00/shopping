package io.cephas.discount;

import io.cephas.buyableitem.BuyableItem;
import io.cephas.buyableitem.ItemId;
import io.cephas.checkout.Checkout;
import io.cephas.money.Money;
import io.cephas.utils.Preconditions;

/**
 * Created by Peter on 17/03/2015.
 * Multi-buy itemId e.g. 2 for £3.50
 */
public class MultibuyDiscount implements Discount
{

    private final Money bulkPrice;
    private final int numberItemForDiscount;
    private final ItemId itemId;

    /**
     * Item to discount
     * @param itemId item
     * @param numberOfItemForDiscount number needed to discount
     * @param bulkPrice price for those items
     */
    public MultibuyDiscount(ItemId itemId, int numberOfItemForDiscount, Money bulkPrice)
    {
        this.itemId = Preconditions.checkNotNull(itemId, "itemId");
        this.numberItemForDiscount = Preconditions.checkMinimumValue(numberOfItemForDiscount, 1, "numberOfItemForDiscount");
        this.bulkPrice = Preconditions.checkNotNull(bulkPrice, "bulkPrice");
    }

    @Override
    public Money getDiscountAmount(Checkout checkout)
    {
        int itemCount = 0;
        for (BuyableItem item : checkout.getScannedItems())
        {
            if (item.getItemId().equals(itemId))
            {
                itemCount += 1;
            }
        }

        if (itemCount == 0)
        {
            return new Money();
        }
        else
        {
            int qualifyingBulkItems = itemCount/numberItemForDiscount;
            // Determine how much the amount would cost without bulk savings
            Money costWithoutBulkSavings = itemId.getCost().multiply(qualifyingBulkItems * numberItemForDiscount);
            Money bulkCost = bulkPrice.multiply(qualifyingBulkItems);
            return costWithoutBulkSavings.subtract(bulkCost);
        }
    }

    @Override
    public ItemId getDiscountItemID()
    {
        return itemId;
    }
}

package io.cephas.discount;

import io.cephas.buyableitem.ItemId;
import io.cephas.checkout.Checkout;
import io.cephas.money.Money;

/**
 * Created by Peter on 17/03/2015.
 *
 * Interface for a discount. Returns the discount to be subtracted from the checkout
 */
public interface Discount
{
    /**
     * Get the amount that should be discounted from the basket
     * @param checkout The checkout containing scanned items
     * @return Amount that should be discounted
     */
    public Money getDiscountAmount(Checkout checkout);

    /**
     * ItemId for the discount deal
     */
    public ItemId getDiscountItemID();
}

package io.cephas.discount;

import io.cephas.buyableitem.BuyableItem;
import io.cephas.buyableitem.ItemId;
import io.cephas.checkout.Checkout;
import io.cephas.money.Money;
import io.cephas.utils.Preconditions;

/**
 * Created by Peter on 17/03/2015.
 * Implementation of a buy X get Y free.
 * E.g. buy 2 meals get 1 free
 */
public class BuyXGetYFreeDiscount implements Discount
{

    private final ItemId itemId;
    private final int numberOfItemForDiscount;
    private final int numberGivenFree;

    /**
     * Item to discount
     * @param itemId item
     * @param numberOfItemForDiscount number needed to discount
     * @param numberGivenFree number of items that are free
     */
    public BuyXGetYFreeDiscount(ItemId itemId, int numberOfItemForDiscount, int numberGivenFree)
    {
        if (numberOfItemForDiscount <= numberGivenFree)
        {
            throw new IllegalArgumentException("numberOfItemForDiscount <= numberGivenFree");
        }

        this.itemId = Preconditions.checkNotNull(itemId, "itemId");
        this.numberOfItemForDiscount = Preconditions.checkMinimumValue(numberOfItemForDiscount, 1, "numberOfItemForDiscount");
        this.numberGivenFree = Preconditions.checkMinimumValue(numberGivenFree, 1, "numberGivenFree");
    }

    @Override
    public Money getDiscountAmount(Checkout checkout)
    {
        int itemCount = 0;
        for (BuyableItem item : checkout.getScannedItems())
        {
            if (item.getItemId().equals(itemId))
            {
                itemCount += 1;
            }
        }
        if (itemCount == 0)
        {
            return new Money();
        }
        else
        {
            int totalNumberFree = (itemCount/ numberOfItemForDiscount) * numberGivenFree;
            return itemId.getCost().multiply(totalNumberFree);
        }
    }

    @Override
    public ItemId getDiscountItemID()
    {
        return itemId;
    }
}

package io.cephas.checkout;

import io.cephas.buyableitem.BuyableItem;
import io.cephas.buyableitem.ItemId;
import io.cephas.discount.Discount;
import io.cephas.money.Money;

import java.util.*;

/**
 * Created by Peter on 17/03/2015.
 * Implementation of a checkout
 */
public class Checkout
{
    private final List<Discount> discountList;

    private final List<BuyableItem> scannedItems;

    private Checkout(List<Discount> discountList)
    {
        this.discountList = discountList;
        this.scannedItems = new ArrayList<BuyableItem>();
    }

    /**
     * Scans an item through the checkout and adds it to the total
     * @param buyableItem the item to be bought
     * @return this
     */
    public Checkout scanItem(BuyableItem buyableItem)
    {
        scannedItems.add(buyableItem);
        System.out.println("Scanning:: " + buyableItem.getItemName() + " @ " + buyableItem.getCost());
        return this;
    }

    /**
     * Gets all items that have been scanned
     */
    public List<BuyableItem> getScannedItems()
    {
        return scannedItems;
    }

    /**
     * Returns the total cost of the items scanned without any discounts applied
     */
    public Money getTotalWithoutDiscount()
    {
        Money totalMoney = new Money(0,0);
        for (BuyableItem item : scannedItems)
        {
            totalMoney = totalMoney.add(item.getCost());
        }

        return totalMoney;
    }

    /**
     * Returns the total cost of the items scanned with discounts
     */
    public Money getTotalWithDiscountApplied()
    {
        return getTotalWithoutDiscount().subtract(getSaving());
    }

    /**
     * Returns the money saved on the items scanned
     */
    public Money getSaving()
    {
        Money totalMoney = new Money(0,0);
        for (Discount discount : discountList)
        {
            totalMoney = totalMoney.add(discount.getDiscountAmount(this));
        }
        return totalMoney;
    }

    /**
     * Checkout builder to setup discounts.
     * Only one discount is possible per {@link ItemId}.
     */
    public static final class Builder
    {
        Map<ItemId, Discount> discountMap;

        public Builder()
        {
            discountMap = new HashMap<ItemId, Discount>();
        }

        /**
         * Adds discounts
         */
        public Builder addDiscount(Discount... discounts)
        {
            for (Discount discount : discounts)
            {
                discountMap.put(discount.getDiscountItemID(), discount);
            }
            return this;
        }

        /**
         * Creates a checkout configured with the discounts
         * @return
         */
        public Checkout build()
        {
            return new Checkout(new ArrayList<Discount>(discountMap.values()));
        }
    }
}

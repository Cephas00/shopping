package io.cephas.buyableitem;

import io.cephas.money.Money;

/**
 * Created by Peter on 17/03/2015.
 * ItemId. Each type of item should have a unique item id e.g. biscuits, meals etc.
 * {@link BuyableItem} can be used to create items that are usable with
 */
public interface ItemId
{
    /**
     * Get the unique ID value for an item
     * @return
     */
    public int getItemIdValue();

    /**
     * Get the base cost of an item
     * @return
     */
    public Money getCost();
}

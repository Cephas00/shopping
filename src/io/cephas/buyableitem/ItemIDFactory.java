package io.cephas.buyableitem;

import io.cephas.money.Money;

/**
 * Created by Peter on 17/03/2015.
 * Factory to create itemids.
 */
public class ItemIDFactory
{
    private int id;

    public ItemIDFactory()
    {
        id = 0;
    }

    /**
     * Creates an {@link ItemId} with a unique Id number
     * @param cost price of the item
     * @return new ItemId
     */
    public ItemId createNextItemID(final Money cost)
    {
        id += 1;
        return new ItemIdImpl(id, cost);
    }
}

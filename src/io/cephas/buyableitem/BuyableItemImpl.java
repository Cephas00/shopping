package io.cephas.buyableitem;

/**
 * Created by Peter on 17/03/2015.
 */

import io.cephas.money.Money;
import io.cephas.utils.Preconditions;

/**
 * Generic implementation of a buyable item
 */
public class BuyableItemImpl implements BuyableItem
{
    private final String itemName;
    private final ItemId itemId;

    public BuyableItemImpl(ItemId itemId, String itemName)
    {
        this.itemId = Preconditions.checkNotNull(itemId, "itemId");
        this.itemName = Preconditions.checkNotNull(itemName, "itemName");
    }

    @Override
    public Money getCost()
    {
        return itemId.getCost();
    }

    @Override
    public String getItemName()
    {
        return itemName;
    }

    @Override
    public ItemId getItemId()
    {
        return itemId;
    }
}

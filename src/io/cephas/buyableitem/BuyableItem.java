package io.cephas.buyableitem;

import io.cephas.money.Money;

/**
 * Created by Peter on 17/03/2015.
 * Interface for a buyable item.
 * ItemId includes the price so we can have multiple types with the same id e.g. 3 different kinds of
 * biscuits but are all still of the {@link ItemId biscuit}
 */
public interface BuyableItem
{
    /**
     * get the base cost of an item
     * @return cost
     */
    public Money getCost();

    /**
     * Gets the item name
     * @return item name
     */
    public String getItemName();

    /**
     * Gets the ID of the item
     * @return item id
     */
    public ItemId getItemId();
}

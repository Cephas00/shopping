package io.cephas.buyableitem;

import io.cephas.money.Money;
import io.cephas.utils.Preconditions;

/**
 * Created by Peter on 17/03/2015.
 * Implementation of an ItemId.
 */
public class ItemIdImpl implements ItemId
{
    private final Money cost;
    private final int value;

    public ItemIdImpl(int itemIdVal, Money cost)
    {
        Preconditions.checkNotNull(cost, "cost");
        Preconditions.checkMinimumValue(itemIdVal, 0, "itemIdVal");
        this.value = itemIdVal;
        this.cost = cost;
    }

    @Override
    public int getItemIdValue()
    {
        return value;
    }

    @Override
    public Money getCost()
    {
        return cost;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemIdImpl itemId = (ItemIdImpl) o;

        if (value != itemId.value) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return value;
    }

}

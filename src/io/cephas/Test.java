package io.cephas;

import io.cephas.buyableitem.BuyableItem;
import io.cephas.buyableitem.BuyableItemImpl;
import io.cephas.buyableitem.ItemIDFactory;
import io.cephas.buyableitem.ItemId;
import io.cephas.checkout.Checkout;
import io.cephas.discount.BuyXGetYFreeDiscount;
import io.cephas.discount.Discount;
import io.cephas.discount.MultibuyDiscount;
import io.cephas.money.Money;
import io.cephas.utils.Preconditions;

/**
 * Created by Peter on 17/03/2015.
 */
public class Test
{

    public static void main(String[] args)
    {
        testEmptyCheckout();
        testXGetY();
        testMultibuy();
    }

    public static void testEmptyCheckout()
    {
        Checkout checkout = new Checkout.Builder().build();
        printCheckoutDetails(checkout);
    }

    public static void testXGetY()
    {
        ItemIDFactory itemIDFactory = new ItemIDFactory();
        ItemId biscuitItemId = itemIDFactory.createNextItemID(new Money(1, 29));
        BuyableItem hobnobsBiscuit = new BuyableItemImpl(biscuitItemId, "Hobnobs");
        BuyableItem bourbonBiscuit = new BuyableItemImpl(biscuitItemId, "Bourbon");
        Discount buyXGetYDiscount = new BuyXGetYFreeDiscount(biscuitItemId, 3, 1);

        Checkout checkout = new Checkout.Builder().addDiscount(buyXGetYDiscount).build();
        checkout.scanItem(hobnobsBiscuit);
        checkout.scanItem(bourbonBiscuit);
        checkout.scanItem(hobnobsBiscuit);

        printCheckoutDetails(checkout);
    }

    public static void testMultibuy()
    {
        ItemIDFactory itemIDFactory = new ItemIDFactory();
        ItemId readyMealId = itemIDFactory.createNextItemID(new Money(3, 50));
        BuyableItem carbonaraMeal = new BuyableItemImpl(readyMealId, "Carbonara");
        BuyableItem lingPieMeal = new BuyableItemImpl(readyMealId, "Ling Pie");
        Discount multiBuyDeal = new MultibuyDiscount(readyMealId, 2, new Money(5,0));

        Checkout checkout = new Checkout.Builder().addDiscount(multiBuyDeal).build();
        checkout.scanItem(carbonaraMeal);
        checkout.scanItem(lingPieMeal);
        checkout.scanItem(lingPieMeal);

        printCheckoutDetails(checkout);
    }

    private static void printCheckoutDetails(Checkout checkout)
    {
        Preconditions.checkNotNull(checkout, "checkout");
        System.out.println("Total without discount: " + checkout.getTotalWithoutDiscount());
        System.out.println("Total with discount: " + checkout.getTotalWithDiscountApplied());
        System.out.println("Total savings: " + checkout.getSaving());
    }
}

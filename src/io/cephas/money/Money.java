/*
 * (c) Aspects Tools Ltd.
 */

package io.cephas.money;

import io.cephas.utils.Preconditions;

/**
 * Class representing money.
 */
public class Money
{
    private final int pounds;

    private final int pennies;

    public Money(int pounds, int pennies)
    {
        this.pounds = Preconditions.checkMinimumValue(pounds, 0, "pounds");
        this.pennies = Preconditions.checkRange(pennies, 0, 99, "pennies");
    }

    public Money()
    {
        this(0,0);
    }

    /**
     * Add provided money to this money. Returns a new instance.
     */
    public Money add(Money money)
    {
        int totalPounds = 0;
        int totalPennies = money.getPennies() + pennies;
        if (totalPennies >= 100)
        {
            totalPounds += 1;
            totalPennies -= 100;
        }
        totalPounds += money.getPounds();
        totalPounds += pounds;

        return new Money(totalPounds, totalPennies);
    }

    /**
     * Subtract provided money from this money. Returns a new instance.
     */
    public Money subtract(Money money)
    {
        int totalPounds = pounds - money.getPounds();
        int totalPennies = pennies - money.getPennies();
        if (totalPennies < 0)
        {
            totalPounds -= 1;
            totalPennies += 100;
        }

        return new Money(totalPounds, totalPennies);
    }

    /**
     * Multiply money by int. Returns a new instance.
     */
    public Money multiply(int x)
    {
        int totalPounds = pounds * x;
        int totalPennies = pennies * x;
        totalPounds += totalPennies/100;
        totalPennies -= (totalPennies/100)*100;
        return new Money(totalPounds, totalPennies);
    }

    /**
     * Gets the pounds part of this cost.
     */
    public int getPounds()
    {
        return pounds;
    }

    /**
     * Gets the pennies part of this cost.
     */
    public int getPennies()
    {
        return pennies;
    }

    @Override
    public String toString()
    {
        String penniesText = pennies <=9 ? "0" + pennies : String.valueOf(pennies);
        return "£" + pounds + "." + penniesText;
    }
}
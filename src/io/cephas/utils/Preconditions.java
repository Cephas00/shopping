package io.cephas.utils;

/**
 * Utility preconditions to use for data parsing
 */
public final class Preconditions
{
    private Preconditions()
    {

    }

    public static int checkRange(int value, int min, int max, Object errorMessage)
    {
        if (value >= min && value<=max)
        {
            return value;
        }
        else
        {
            throw new IllegalArgumentException(String.valueOf(errorMessage) + " not in range");
        }
    }

    /**
     * Check if an object is null
     * @param obj object to check for null
     * @param errorMessage error message which will be displayed using {@link String#valueOf(java.lang.Object)}
     * @return non null reference
     */
    public static <T> T checkNotNull(T obj, Object errorMessage)
    {
        if (obj == null)
        {
            throw new NullPointerException(String.valueOf(errorMessage) + " is null");
        }
        else
        {
            return obj;
        }
    }

    public static int checkMinimumValue(int value, int minimumValue, Object errorMessage)
    {
        if (value >= minimumValue)
        {
            return value;
        }
        else
        {
            throw new IllegalArgumentException(String.valueOf(errorMessage) + " is not greater than or equal to " + minimumValue);
        }
    }

}
